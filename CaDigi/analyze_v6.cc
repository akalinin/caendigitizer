#include "TH1.h" 
#include "TF1.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TPaveStats.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TPolyMarker.h"
#include "TSpectrum.h"
#include "TTree.h"
#include "TString.h"
#include "string.h"


#include "analyze.h"

#include <fstream>
#include <iostream>
#include <dirent.h>
#include <sstream>


using namespace std;

// n0 vas not initialized
int n0;

double generpoiss(double *x, double *p){
  /* Function description is here - https://indico.cern.ch/event/725114/contributions/2983038/attachments/1641357/2621413/he_sipm_dpg2704_v2.pdf#page=6 */
  double xx = x[0];
  double u0 = p[4];
  double s0 = p[5];
  double g = p[1];
  double sG = p[2];
  double u = p[3];
  double lambda = p[0];
  double N = p[6];
  double npeaks = p[7];
  double fitval = 0.;

  for (int k=0; k<npeaks+1; k++) {
    double uk = u + k*lambda;
    double G = u * pow(uk, k-1) * exp(-uk) / TMath::Factorial(k);
    double sk2 = s0*s0 + k*sG*sG;
    double dx = xx - u0 - k*g;
    fitval += N * G * exp(-0.5*dx*dx/sk2) / sqrt(2*TMath::Pi()) / sqrt(sk2);
  }
  return fitval;
}

struct GenPoisParams{
  GenPoisParams():gain(0),u0(0),s0(0),sG(0),lambda(0),navg(0),npeaks(0),gain_spe_01(0),gs0(0),gs1(0),gm0(0),gm1(0),gN0(0),gN1(0),g0Chi2(0), g1Chi2(0), g0NDF(0), g1NDF(0),g0Integral(0),g1Integral(0){}

  /* GP fit */
  double gain, u0, s0, sG, lambda, navg, npeaks;

  /* Gaussian fits */
  double gain_spe_01, gs0, gs1, gm0, gm1;
  double gN0, gN1;
  double g0Chi2, g1Chi2, g0NDF, g1NDF;
  /* Integrals of gaussian fits */
  double g0Integral, g1Integral;

  /* Info from histogram */

} fp;

TF1 *GP(TH1F* hist2proc, int xlow=0, int xhigh=500){
  double npe_peaks, mu_hist, par[6], lastbin_x;
  double binwidth;
  int ntotal;

  //hist2proc -> GetXaxis() -> UnZoom();
  binwidth=hist2proc->GetBinWidth(1);
  // uncomment this for day 7
  //xhigh = hist2proc-> GetBinCenter(hist2proc->FindLastBinAbove(0,1)); // in case of only GP in histogram
  hist2proc -> GetXaxis() -> SetRangeUser(xlow, xhigh);
  ntotal=hist2proc->Integral();
  cout<<"Spectrum created"<<endl;
  // Find all peaks and write their positions to vector
  TSpectrum s(15);
  int nfound = s.Search(hist2proc, 1.5, "golf", 0.15);
  double *_xpeaks = s.GetPositionX();
  std::vector<float> xpeaks;
  xpeaks.clear();
  if (_xpeaks==nullptr) {
    cout << "_xpeaks is nullptr" << endl;
  } else {
    for (int p = 0; p < nfound; p++) xpeaks.push_back(_xpeaks[p]);
  }

  std::sort(xpeaks.begin(), xpeaks.end());
 cout<<"Gauss fiting"<<endl;
  // Fit first two peaks with Gauss function. Fit range is 1/3 of distance between two peaks
  TF1 g1("peak0","gaus",xpeaks[0]-(xpeaks[1]-xpeaks[0])/3,xpeaks[0]+(xpeaks[1]-xpeaks[0])/3);
  hist2proc -> Fit(&g1,"RQ");
  TF1 g2("peak1","gaus",xpeaks[1]-(xpeaks[1]-xpeaks[0])/3,xpeaks[1]+(xpeaks[1]-xpeaks[0])/3);
  hist2proc -> Fit(&g2,"RQ+");

  fp.g1Chi2 = g2.GetChisquare();
  fp.g0NDF = g1.GetNDF();
  fp.g1NDF = g2.GetNDF();
  fp.g0Integral = g1.Integral(xpeaks[0]-(xpeaks[1]-xpeaks[0])/3,xpeaks[0]+(xpeaks[1]-xpeaks[0])/3);
  fp.g1Integral = g2.Integral(xpeaks[1]-(xpeaks[1]-xpeaks[0])/3,xpeaks[1]+(xpeaks[1]-xpeaks[0])/3);


  g1.SetLineColor(kGreen+2);
  g2.SetLineColor(kGreen+2);
  g1.GetParameters(&par[0]);
  g2.GetParameters(&par[3]);

  // Get mean and sigma of two Gaussians
  fp.gN0 = g1.GetParameter(0); // g1params[0]
  fp.gm0 = g1.GetParameter(1);
  fp.gs0 = g1.GetParameter(2);
  fp.gN1 = g2.GetParameter(0);
  fp.gm1 = g2.GetParameter(1);
  fp.gs1 = g2.GetParameter(2);
  fp.gain_spe_01 = fp.gm1 - fp.gm0;

  // Set new range for histogram (Mu1-Mu0)/2 around Mu0
  hist2proc->GetXaxis()->SetRangeUser(fp.gm0-(fp.gm1-fp.gm0)/2,fp.gm0+(fp.gm1-fp.gm0)/2);
  n0 = hist2proc -> Integral();
  hist2proc->GetXaxis()->SetRangeUser(xlow, xhigh);
  npe_peaks = TMath::Nint((xhigh-fp.gm0)/(fp.gm1-fp.gm0));

  mu_hist= -TMath::Log(1.*n0/ntotal);

  // Construct GP function in range [Mu0-(Mu1-Mu0)/2, xhigh]
  TF1 *func = new TF1("generpoiss", generpoiss, fp.gm0-(fp.gm1-fp.gm0)/2, xhigh, 8);
  // Set initial parameters for GP and limits for them
  func -> SetParameters( 0.1, fp.gm1-fp.gm0, sqrt(abs(fp.gs1*fp.gs1 - fp.gs0*fp.gs0)), mu_hist, fp.gm0, fp.gs0, binwidth*ntotal, npe_peaks); //initial parameters
  func -> SetParLimits(0, 0., 1.);
  func -> FixParameter(7, npe_peaks);
  func -> SetParNames("lambda", "gain_SPE", "sigma_gain", "mu_avg", "mu0", "sigma0", "NxW", "npe_peaks");
  func -> SetNpx(1000);
  cout<<"GP fiting"<<endl;
  hist2proc -> Fit(func, "R+");

  // put parameters of GP to fp structure
  fp.gain = func->GetParameter(1);
  fp.u0 = func->GetParameter(4);
  fp.s0 = func->GetParameter(5);
  fp.sG = func->GetParameter(2);
  fp.lambda = func->GetParameter(0);
  fp.navg = func->GetParameter(3);
  fp.npeaks = func->GetParameter(7);

  //for (int i_par = 0; i_par < 8; i_par++) {
  //  cout << func->GetParName(i_par) << " = " << func->GetParameter(i_par) << endl;
  //}

  // return GP function
  return (func);
}

double landau_gauss(double *x, double *par) {
  
  //Fit parameters:
  //par[0]=Width (scale) parameter of Landau density
  //par[1]=Most Probable (MP, location) parameter of Landau density
  //par[2]=Total area (integral -inf to inf, normalization constant)
  //par[3]=Width (sigma) of convoluted Gaussian function
  //
  //In the Landau distribution (represented by the CERNLIB approximation),
  //the maximum is located at x=-0.22278298 with the location parameter=0.
  //This shift is corrected within this function, so that the actual
  //maximum is identical to the MP parameter.

  // Numeric constants
  Double_t invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
  Double_t mpshift = -0.22278298;       // Landau maximum location

  // Control constants
  Double_t np = 1000.0;      // number of convolution steps
  Double_t sc = 6.0;      // convolution extends to +-sc Gaussian sigmas

  // Variables
  Double_t xx;
  Double_t mpc;
  Double_t fland;
  Double_t sum = 0.0;
  Double_t xlow, xupp;
  Double_t step;
  Double_t i;

  // MP shift correction
  mpc = par[1] - mpshift * par[0];

  // Range of convolution integral
  xlow = x[0] - sc * par[3];
  xupp = x[0] + sc * par[3];

  step = (xupp - xlow) / np;

  // Convolution integral of Landau and Gaussian by sum
  for (i = 1.0; i <= np / 2; i++) {
    xx = xlow + (i - .5) * step;
    fland = TMath::Landau(xx, mpc, par[0]) / par[0];
    sum += fland * TMath::Gaus(x[0], xx,  par[3]);

    xx = xupp - (i - .5) * step;
    fland = TMath::Landau(xx, mpc, par[0]) / par[0];
    sum += fland * TMath::Gaus(x[0], xx, par[3]);
  }

  return (par[2] * step * sum * invsq2pi / (par[3]));
}

TF1* LGFit(TH1F *hist) {


  double max = hist->GetMaximum();
  double lower_bound = hist->GetXaxis()->GetBinLowEdge(hist->FindFirstBinAbove(max/10,1,5,-1));
  double upper_bound = hist->GetXaxis()->GetBinUpEdge(hist->FindLastBinAbove(2*max/3));
  int gnpar = 3, lgnpar = 4;
  double gparameters[gnpar],parameters[lgnpar];

  //par[0]=Width (scale) parameter of Landau density
  //par[1]=Most Probable (MP, location) parameter of Landau density
  //par[2]=Total area (integral -inf to inf, normalization constant)
  //par[3]=Width (sigma) of convoluted Gaussian function

  std::cout << "Starting 1 Gaussian Fit...." << std::endl;
  std::cout << std::endl;

  TF1 *G_func = new TF1("1 fit","gaus",lower_bound, upper_bound);
  hist->Fit(G_func,"RQ");
  G_func->GetParameters(gparameters);

  std::cout << "Gaussian Fit is Done. Parameters:" << std::endl;
  std::cout << "Maximum Gauss = " << gparameters[0] << std::endl;
  std::cout << "Mean Gauss = " << gparameters[1] << std::endl;
  std::cout << "Sigma Gauss = " << gparameters[2] << std::endl;
  std::cout << std::endl;

  double Chi2 = G_func->GetChisquare();
  double NDF = G_func->GetNDF();

  std::cout << "Chi^2/NDF is " << Chi2 << " / " << NDF << std::endl;
  std::cout << std::endl;

  parameters[0] = gparameters[2];//hist->GetStdDev();
  parameters[1] = gparameters[1];
  parameters[2] = hist->Integral("width");
  parameters[3] = gparameters[2];

  lower_bound = hist->GetXaxis()->GetBinLowEdge(hist->FindFirstBinAbove(max/10,1,5,-1));
  upper_bound = hist->GetXaxis()->GetBinUpEdge(hist->FindLastBinAbove(max/10));

  std::cout << "Starting 2 LanGaussian Fit...." << std::endl;
  std::cout << std::endl;  

  TF1 *LG_func = new TF1("Landau-Gauss Function", landau_gauss, lower_bound, upper_bound, lgnpar);
  LG_func->SetParameters(parameters);
  //LG_function->ReleaseParameter(0);
  //LG_func->SetParLimits(0, 10, 5000);
  //LG_func->SetParLimits(3, 0, 500000);
  LG_func->FixParameter(3, TMath::Sqrt(1049.09 + 27.53166 * gparameters[1]));
  LG_func->SetLineColor(kGreen);
  hist->Fit(LG_func,"RQB+");
  LG_func->GetParameters(parameters);

  std::cout << "LanGaussian Fit is Done. Parameters:" << std::endl;
  std::cout << "Width Landau = " << parameters[0] << std::endl;
  std::cout << "Position Location Landau = " << parameters[1] << std::endl;
  std::cout << "Integral LanGauss = " << parameters[2] << std::endl;
  std::cout << "Sigma Gauss = " << parameters[3] << std::endl;
  std::cout << std::endl;
  
  Chi2 = LG_func->GetChisquare();
  NDF = LG_func->GetNDF();

  std::cout << std::endl;
  std::cout << "Chi^2/NDF is " << Chi2 << " / " << NDF << std::endl;
  
  return LG_func;

}

TF1* GFit(TH1F *hist) {

  double max = hist->GetMaximum();
  double lower_bound = hist->GetXaxis()->GetBinLowEdge(hist->FindFirstBinAbove(max/10,1,5,-1));
  double upper_bound = hist->GetXaxis()->GetBinUpEdge(hist->FindLastBinAbove(2*max/3));
  double gparameters[3];

 
  TF1 *G_func = new TF1("1 fit","gaus",lower_bound, upper_bound);
  hist->Fit(G_func,"RQ");
  //G_func->SetLineColor(kGreen+2);
  G_func->GetParameters(gparameters);

  for (int ipar=0;ipar<3;ipar++) {
    std::cout << "par" << ipar <<  " = " << gparameters[ipar] << std::endl;
  }

  return G_func;

}

