#include <iostream>
#include <time.h>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "TTree.h"
#include "TFile.h"
#include "TKey.h"
#include "TH1F.h"
#include "TProfile.h"
#include "THttpServer.h"
#include <CAENDigitizer.h>
#include "ConfigFile.h"
#include <signal.h>

TProfile *hwavepr[8];
TProfile *hwavepr2[8];
TH1F *grwave[8],*grwave2[8];
TH1F *hbl[8],*hch[8];
TH1F *htrigger0,*htrigger2,*hsignal,*hbaseline,*hbaseline0,*hcharge,*hcharge0;
int blmin_db[8],blmax_db[8];
int chmin_db[8],chmax_db[8];
int handle;
THttpServer *serv;
const char *PORT ="9000";
char *gBuffer = NULL;
uint32_t buffsize;
uint32_t numEvents;    
uint32_t AllocatedSize;
  bool end=false;

#define MAXNB 1
#define MaxNChannels 8
CAEN_DGTZ_DPP_PSD_Event_t       *Events[MaxNChannels];  // events buffer
//void *Events[MaxNChannels]; 
CAEN_DGTZ_DPP_PSD_Waveforms_t   *Waveform=NULL;         // waveforms buffer
TFile* file;
TTree* tree;

uint32_t NumEvents[MaxNChannels];

int programm_digitizer(char *cfgfilename);
int config_window(char *cfgfilename);
int poll_digitizer();
int read_digitizer();
int analize_buf(int createhsit,int windowwidth);
void intHandler(int dummy);
int main(int argc, char *argv[])

{
  signal(SIGINT, intHandler);
  ConfigFile config(argv[1]);
   programm_digitizer(argv[1]);
   config_window(argv[1]);
  int Nevents = config.read<int>("nevents");
  int Actime = config.read<int>("actime");
  serv=new THttpServer("http:9000");
  //serv->SetItemField("/", "_monitoring", "100");
  serv->SetItemField("/", "_layout", "grid2x2");
  //serv->SetTimer(0, kTRUE);
  //serv->CreateEngine("http:9000");
  //serv->SetJSROOT("/home/online/packages/jsroot");
  char sname[200];

  int ch;
  int nevents=0;
  int actime=0;
  //serv->SetReadOnly(kFALSE);
 int windowwidth = config.read<int>("windowwidth");
int samples = config.read<int>("samples");
   sprintf(sname,"trigger-0");
    htrigger0=new TH1F(sname,sname,samples,0,samples);
    printf("%s created!\n",sname);
    sprintf(sname,"trigger-2");
    htrigger2=new TH1F(sname,sname,samples,0,samples);
    printf("%s created!\n",sname);
    sprintf(sname,"signal");
    hsignal=new TH1F(sname,sname,samples,0,samples);
    printf("%s created!\n",sname);
    /*
    sprintf(sname,"baseline");
    hbaseline=new TH1F(sname,sname,windowwidth*3000,windowwidth*7000,windowwidth*10000);
    printf("%s created!\n",sname);
    */
    sprintf(sname,"baseline-0");
    hbaseline0=new TH1F(sname,sname,windowwidth*3000,windowwidth*(-1000),windowwidth*2000);
    printf("%s created!\n",sname);
    /*
    sprintf(sname,"charge");
    hcharge=new TH1F(sname,sname,windowwidth*10000,windowwidth*(7000),windowwidth*10000);
    printf("%s created!\n",sname);
    */
    sprintf(sname,"charge-0");
    hcharge0=new TH1F(sname,sname,windowwidth*3000,windowwidth*(-1000),windowwidth*2000);
    printf("%s created!\n",sname);

  for(ch=0;ch<8;ch++) {
    sprintf(sname,"wavepr%d",ch);
    hwavepr[ch]=new TProfile(sname,sname,4000,0,4000);
    sprintf(sname,"wave2pr%d",ch);
    hwavepr2[ch]=new TProfile(sname,sname,4000,0,4000);
    printf("%s created!\n",sname);
    sprintf(sname,"baseline%d",ch);
    hbl[ch]=new TH1F(sname,sname,10000,600000,700000);
    printf("%s created!\n",sname);
    sprintf(sname,"charge%d",ch);
    hch[ch]=new TH1F(sname,sname,10000,600000,700000);
    printf("%s created!\n",sname);
    sprintf(sname,"grwave-%d",ch);
    grwave[ch]=new TH1F(sname,sname,4000,0,4000);
    sprintf(sname,"grwave2-%d",ch);
    grwave2[ch]=new TH1F(sname,sname,4000,0,4000);
    printf("%s created!\n",sname);
    // serv->Register("profiles",hwavepr[ch]);
  }
  int iloop=0;
  CAEN_DGTZ_SWStartAcquisition(handle);

  while(!end)
    {
      if (iloop==10000000)
	{
	  std::cout<<"iloop"<<std::endl;
	  iloop=0;
	}
      if (end) continue;
      if (poll_digitizer()==1) 
	{
	  //usleep(1000);  
	  if (end) continue;
	  if(read_digitizer()!=0){ 
	    system("clear");
	    std::cout<<"Reading "<<nevents<<std::endl;
	  //read_digitizer();
	  	   for (int i=0;i<8;i++) hwavepr[i]->Reset();
	  	   for (int i=0;i<8;i++) hwavepr2[i]->Reset();
		   for (int i=0;i<8;i++) grwave[i]->Reset();
	  	   for (int i=0;i<8;i++) grwave2[i]->Reset();
		   htrigger0->Reset();
		   htrigger2->Reset();
		   hsignal->Reset();
	  //usleep(10);
	    if (end) continue;
	    analize_buf(nevents,windowwidth);
	  nevents++;
	  }
	}
    
      if ((nevents>Nevents)&&(Nevents!=0)) end=true;
      serv->ProcessRequests();
      // for (int i=0;i<8;i++) hwavepr[i]->Reset();
      iloop++;
    }
  CAEN_DGTZ_SWStopAcquisition(handle);
  CAEN_DGTZ_CloseDigitizer(handle);
  printf("CloseDigitizer\n");
  CAEN_DGTZ_FreeReadoutBuffer(&gBuffer);
  CAEN_DGTZ_FreeDPPEvents(handle, (void**)Events);
  CAEN_DGTZ_FreeDPPWaveforms(handle, (void**)Waveform);
 std::cout<<"Finished events="<<nevents<<std::endl;
 sleep(5);
  return 0;
}


int poll_digitizer()
{ 
  // return 1;
  int flag = 1;
  uint32_t Data;
  CAEN_DGTZ_ReadRegister(handle,0x812c,&Data);
  if(Data > 0) flag = 1; else flag=0;
  return flag;
}
int analize_buf(int createhists,int windowwidth)
{
  for(int ch=0; ch<MaxNChannels; ch++) {
    std::cout<<"ch="<<ch<<" Numevents="<<NumEvents[ch]<<std::endl;
    for(int ev=0; ev<NumEvents[ch]; ev++) {
     
    // for(int ev=0; ev<1; ev++) {
      if (ev<5)printf("%x %x %x %d\n",ch,Events[ch][ev].Format,Events[ch][ev].Format2,Events[ch][ev].TimeTag);
      //   *pdata++=ch;
      //*pdata++ = Events[ch][ev].TimeTag;
      //*pdata++=Events[ch][ev].ChargeLong;
      //*pdata++=Events[ch][ev].ChargeShort;
      //   hch[ch]->Fill(Events[ch][ev].ChargeLong);
      //   hbl[ch]->Fill(Events[ch][ev].Baseline);
      CAEN_DGTZ_DecodeDPPWaveforms(handle, &Events[ch][ev], (void**)Waveform);
      int size = (int)(Waveform->Ns);
      int32_t *WD = (int32_t*)Waveform;
      //for(int i=0;i<10;i++)printf("%d\t",WD[i]);
      //  *pdata++=size;
      int baseline=0;
      int charge=0;
      int16_t *WaveData = (int16_t*)Waveform->Trace1;
      for(int i=0; i<size; i++){
	if((i>=blmin_db[ch])&&(i<=blmax_db[ch])) baseline+=WaveData[i];
	if((i>=chmin_db[ch])&&(i<=chmax_db[ch])) charge+=WaveData[i];
	if (ev==10) {
	if ((Events[ch][ev].TimeTag==Events[0][ev].TimeTag))hwavepr[ch]->Fill(i,WaveData[i]);	
	//	if ((Events[ch][ev].TimeTag==Events[2][ev].TimeTag))
hwavepr2[ch]->Fill(i,WaveData[i]);
 grwave2[ch]->SetBinContent(i+1,WaveData[i]);
 if (ch==0) htrigger0->SetBinContent(i+1,WaveData[i]);
 if (ch==2) htrigger2->SetBinContent(i+1,WaveData[i]);
 if (ch==4) hsignal->SetBinContent(i+1,WaveData[i]);
	}
	//*pdata++=WaveData[i];
      }
      if (ch==4){
	if (createhists!=0){	
	    hbaseline->Fill(baseline);
	    hcharge->Fill(charge);
	}
	if ((createhists==0)&&(!hbaseline)){
	  char sname[200];
	  sprintf(sname,"baseline");
	  hbaseline=new TH1F(sname,sname,windowwidth*3000,baseline-windowwidth*(1000),baseline+windowwidth*(2000));
	  printf("%s created!\n",sname);
	  sprintf(sname,"charge");
	  hcharge=new TH1F(sname,sname,windowwidth*3000,baseline-windowwidth*(1000),baseline+windowwidth*(2000));
	  printf("%s created!\n",sname);
	}
      hbaseline0->Fill(baseline-baseline);
      hcharge0->Fill(charge-baseline);
      }
        hch[ch]->Fill(charge);
         hbl[ch]->Fill(baseline);
	 //	 printf("ch=%d charge=%d\n",ch,charge);
    }
  }

  return 1;
}
int read_digitizer()
{ 
   int ret;
   int ret2 =  CAEN_DGTZ_ReadData(handle, CAEN_DGTZ_SLAVE_TERMINATED_READOUT_MBLT, gBuffer, &buffsize);
   //printf("read caen event %d\n",ret2);
   uint32_t * words = (uint32_t*)gBuffer;
   uint32_t nEvtSze;
   int i,ch,ev;
   if (ret2<0) return 0;
   //  uint32_t NumEvents[MaxNChannels];
   ret =CAEN_DGTZ_GetDPPEvents(handle,gBuffer,buffsize,(void **)Events,NumEvents);
   if ((ret<0)||(buffsize==0)) return 0;
   printf("getDPPEvents ret=%d,buffsize=%d\n",ret,buffsize);
   for(i=0;i<MaxNChannels;i++)printf("events[%d]=%d\n",i,NumEvents[i]);
   // std::cout<<"read_digitizer"<<std::endl;
  return 1;
}


int config_window(char *cfgfilename)
{

ConfigFile config(cfgfilename);
 TString blmin = config.read<string>("blmin").data();
   int windowwidth = config.read<int>("windowwidth");
  blmin +=","; //needed for seperation of channels
 
  TString eval;
  int counter= 0;
  for (int i=0; i< blmin.Length(); i++){
    if(blmin[i]!=',')				// if the element isn't a comma,
      eval.Append(blmin[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	blmin_db[counter]=element;
	counter ++;// append the integer to the array
      }	
      }

 TString blmax = config.read<string>("blmax").data();
  
  blmax +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< blmax.Length(); i++){
    if(blmax[i]!=',')				// if the element isn't a comma,
      eval.Append(blmax[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	blmax_db[counter]=element;
	counter ++;// append the integer to the array
      }	
      }
 TString chmax = config.read<string>("chmax").data();
  
  chmax +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< chmax.Length(); i++){
    if(chmax[i]!=',')				// if the element isn't a comma,
      eval.Append(chmax[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	chmax_db[counter]=element;
	counter ++;// append the integer to the array
      }	
      }

 TString chmin = config.read<string>("chmin").data();
  
  chmin +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< chmin.Length(); i++){
    if(chmin[i]!=',')				// if the element isn't a comma,
      eval.Append(chmin[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	chmin_db[counter]=element;
	counter ++;// append the integer to the array
      }	
      }
  if (windowwidth!=0)
    {
      for (int i=0;i<8;i++){
	blmax_db[i]=blmin_db[i]+windowwidth;
	chmax_db[i]=chmin_db[i]+windowwidth;
      }
    }

  return 1;
}



int programm_digitizer(char *cfgfilename)
{
  




  /* The following variable will be used to get an handler for the digitizer. The
  handler will be used for most of CAENDigitizer functions to identify the board */
  ConfigFile config(cfgfilename);
  char str[256];
  //HNDLE hKey, hRootKey, hSubkey;
  int threash_db[8];
  int nsbl_db[8];
  int lgate_db[8];
  int sgate_db[8];
  int pgate_db[8];
 int selft_db[8];
 int pretr_db[8];
 int polar_db[8];
 int ret;
 
  TString threash = config.read<string>("treash").data();
  
  threash +=","; //needed for seperation of channels
 
  TString eval;
  int counter= 0;
  for (int i=0; i< threash.Length(); i++){
    if(threash[i]!=',')				// if the element isn't a comma,
      eval.Append(threash[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	threash_db[counter]=element;
	counter ++;// append the integer to the array
      }	
      }
  TString nsbl = config.read<string>("nsbl").data();
 
  nsbl +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< nsbl.Length(); i++){
    if(nsbl[i]!=',')				// if the element isn't a comma,
      eval.Append(nsbl[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	nsbl_db[counter]=element;
	counter ++;// append the integer to the array
      }	
  }
  TString lgate = config.read<string>("lgate").data();
 
  lgate +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< lgate.Length(); i++){
    if(lgate[i]!=',')				// if the element isn't a comma,
      eval.Append(lgate[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	lgate_db[counter]=element;
	counter ++;// append the integer to the array
      }	
  }
  TString sgate = config.read<string>("sgate").data();
 
  sgate +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< sgate.Length(); i++){
    if(sgate[i]!=',')				// if the element isn't a comma,
      eval.Append(sgate[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	sgate_db[counter]=element;
	counter ++;// append the integer to the array
      }	
  }

  TString pgate = config.read<string>("pgate").data();
 
  pgate +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< pgate.Length(); i++){
    if(pgate[i]!=',')				// if the element isn't a comma,
      eval.Append(pgate[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	pgate_db[counter]=element;
	counter ++;// append the integer to the array
      }	
  }

  TString selft = config.read<string>("selft").data();
 
  selft +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< selft.Length(); i++){
    if(selft[i]!=',')				// if the element isn't a comma,
      eval.Append(selft[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	selft_db[counter]=element;
	counter ++;// append the integer to the array
      }	
  }

  TString pretr = config.read<string>("pretr").data();
 
  pretr +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< pretr.Length(); i++){
    if(pretr[i]!=',')				// if the element isn't a comma,
      eval.Append(pretr[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	pretr_db[counter]=element;
	counter ++;// append the integer to the array
      }	
  }


  TString polar = config.read<string>("polarity").data();
 
  polar +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< polar.Length(); i++){
    if(polar[i]!=',')				// if the element isn't a comma,
      eval.Append(polar[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	polar_db[counter]=element;
	counter ++;// append the integer to the array
      }	
  }

  for (int i=0;i<8;i++)printf("channel %d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n",i,threash_db[i],nsbl_db[i],lgate_db[i],sgate_db[i],pgate_db[i],selft_db[i],pretr_db[i]);

  CAEN_DGTZ_BoardInfo_t BoardInfo;
  CAEN_DGTZ_EventInfo_t eventInfo;
  CAEN_DGTZ_UINT16_EVENT_t *Evt = NULL;
  int i,b,ch;
  int c = 0, count[MAXNB];
  char * evtptr = NULL;
  uint32_t size,bsize;
  //uint32_t numEvents;
CAEN_DGTZ_DPP_PSD_Params_t DPPParams;
 memset(&DPPParams, 0, sizeof(CAEN_DGTZ_DPP_PSD_Params_t));
 for(ch=0; ch<MaxNChannels; ch++){
            DPPParams.thr[ch] = threash_db[ch];        // Trigger Threshold
            /* The following parameter is used to specifiy the number of samples for the baseline averaging:
            0 -> absolute Bl
            1 -> 16samp
            2 -> 64samp
            3 -> 256samp
            4 -> 1024samp */
            DPPParams.nsbl[ch] = nsbl_db[ch];
            DPPParams.lgate[ch] = lgate_db[ch];    // Long Gate Width (N*2ns for x730  and N*4ns for x725)
            DPPParams.sgate[ch] = sgate_db[ch];    // Short Gate Width (N*2ns for x730  and N*4ns for x725)
            DPPParams.pgate[ch] = pgate_db[ch];     // Pre Gate Width (N*2ns for x730  and N*4ns for x725) 
            /* Self Trigger Mode:
            0 -> Disabled
            1 -> Enabled */
            DPPParams.selft[ch] = selft_db[ch];
            // Trigger configuration:
            //    CAEN_DGTZ_DPP_TriggerMode_Normal ->  Each channel can self-trigger independently from the other channels
			//    CAEN_DGTZ_DPP_TriggerMode_Coincidence -> A validation signal must occur inside the shaped trigger coincidence window
	    // DPPParams.trgc[ch] = CAEN_DGTZ_DPP_TriggerConfig_t(CAEN_DGTZ_DPP_TriggerMode_Normal);

			/*Discrimination mode for the event selection 
			CAEN_DGTZ_DPP_DISCR_MODE_LED -> Leading Edge Distrimination
			CAEN_DGTZ_DPP_DISCR_MODE_CFD -> Constant Fraction Distrimination*/
			DPPParams.discr[ch] = CAEN_DGTZ_DPP_DISCR_MODE_LED;

			/*CFD delay (N*2ns for x730  and N*4ns for x725)  */
			//	DPPParams.cfdd[ch] = 500;  

			/*CFD fraction: 0->25%; 1->50%; 2->75%; 3->100% */
			DPPParams.cfdf[ch] = 0;

            /* Trigger Validation Acquisition Window */
            DPPParams.tvaw[ch] = 2;

            /* Charge sensibility: 
			Options for Input Range 2Vpp: 0->5fC/LSB; 1->20fC/LSB; 2->80fC/LSB; 3->320fC/LSB; 4->1.28pC/LSB; 5->5.12pC/LSB 
			Options for Input Range 0.5Vpp: 0->1.25fC/LSB; 1->5fC/LSB; 2->20fC/LSB; 3->80fC/LSB; 4->320fC/LSB; 5->1.28pC/LSB */
            DPPParams.csens[ch] = 3;
        }
    /* Pile-Up rejection Mode
        CAEN_DGTZ_DPP_PSD_PUR_DetectOnly -> Only Detect Pile-Up
        CAEN_DGTZ_DPP_PSD_PUR_Enabled -> Reject Pile-Up */
   DPPParams.purh = CAEN_DGTZ_DPP_PSD_PUR_DetectOnly;
   //DPPParams.purh = CAEN_DGTZ_DPP_PSD_PUR_Enabled;
	  DPPParams.purgap = 100;  // Purity Gap in LSB units (1LSB = 0.12 mV for 2Vpp Input Range, 1LSB = 0.03 mV for 0.5 Vpp Input Range )
	  //  DPPParams.blthr = 3;     // Baseline Threshold
	int trho = config.read<int>("trho");
	   DPPParams.trgho = trho;     // Trigger HoldOff
	   std::cout<<"trho="<<trho<<std::endl;
 //i = sizeof(CAEN_DGTZ_TriggerMode_t);
 // for(b=0; b<MAXNB; b++){
  /* IMPORTANT: The following function identifies the different boards with a system which may change
  for different connection methods (USB, Conet, ecc). Refer to CAENDigitizer user manual for more info.
  brief:
  CAEN_DGTZ_OpenDigitizer(<LikType>, <LinkNum>, <ConetNode>, <VMEBaseAddress>, <*handler>);
  Some examples below */
 
  /* The following is for b boards connected via b USB direct links
  in this case you must set <LikType> = CAEN_DGTZ_USB and <ConetNode> = <VMEBaseAddress> = 0 */
  ret = CAEN_DGTZ_OpenDigitizer(CAEN_DGTZ_USB, 0, 0, 0, &handle);
 
  /* The following is for b boards connected via 1 opticalLink in dasy chain
  in this case you must set <LikType> = CAEN_DGTZ_PCI_OpticalLink and <LinkNum> = <VMEBaseAddress> = 0 */
  //ret = CAEN_DGTZ_OpenDigitizer(Params[b].LinkType, 0, b, Params[b].VMEBaseAddress, &handle[b]);
 
  /* The following is for b boards connected to A2818 (or A3818) via opticalLink (or USB with A1718)
  in this case the boards are accessed throught VME bus, and you must specify the VME address of each board:
  <LikType> = CAEN_DGTZ_PCI_OpticalLink (CAEN_DGTZ_PCIE_OpticalLink for A3818 or CAEN_DGTZ_USB for A1718)
  <LinkNum> must be the bridge identifier
  <ConetNode> must be the port identifier in case of A2818 or A3818 (or 0 in case of A1718)
  <VMEBaseAddress>[0] = <0xXXXXXXXX> (address of first board)
  <VMEBaseAddress>[1] = <0xYYYYYYYY> (address of second board)
  ...
  <VMEBaseAddress>[b-1] = <0xZZZZZZZZ> (address of last board)
  See the manual for details */
  //ret = CAEN_DGTZ_OpenDigitizer(CAEN_DGTZ_USB,0,0,0x32100000,&handle[b]);
  if(ret != CAEN_DGTZ_Success) {
  printf("Can't open digitizer\n");
  //goto QuitProgram;
  }
  /* Once we have the handler to the digitizer, we use it to call the other functions */
  ret = CAEN_DGTZ_GetInfo(handle, &BoardInfo);
  printf("\nConnected to CAEN Digitizer Model %s, recognized as board %d\n", BoardInfo.ModelName, 0);
  printf("\tROC FPGA Release is %s\n", BoardInfo.ROC_FirmwareRel);
  printf("\tAMC FPGA Release is %s\n", BoardInfo.AMC_FirmwareRel);
 
  ret = CAEN_DGTZ_Reset(handle); /* Reset Digitizer */
  ret = CAEN_DGTZ_GetInfo(handle, &BoardInfo); /* Get Board Info */
  int samples = config.read<int>("samples");
  ret = CAEN_DGTZ_SetRecordLength(handle,samples); /* Set the lenght of each waveform (in samples) */
  //ret = CAEN_DGTZ_SetChannelEnableMask(handle[b],1); /* Enable channel 0 */


  //ret = CAEN_DGTZ_SetChannelTriggerThreshold(handle,0,50); /* Set selfTrigger threshold */
  //ret = CAEN_DGTZ_SetChannelTriggerThreshold(handle[0],1,50);
  //ret = CAEN_DGTZ_SetChannelSelfTrigger(handle,CAEN_DGTZ_TRGMODE_ACQ_ONLY,0); /* Set trigger on channel 0 to be ACQ_ONLY */
  ret = CAEN_DGTZ_SetSWTriggerMode(handle,CAEN_DGTZ_TRGMODE_ACQ_ONLY); /* Set the behaviour when a SW tirgger arrives */
  //ret = CAEN_DGTZ_SetMaxNumEventsBLT(handle,1); /* Set the max number of events to transfer in a sigle readout */
  ret = CAEN_DGTZ_SetAcquisitionMode(handle,CAEN_DGTZ_SW_CONTROLLED); /* Set the acquisition mode */
  //ret = CAEN_DGTZ_SetDPPAcquisitionMode(handle[b],CAEN_DGTZ_DPP_ACQ_MODE_Mixed);
  //ret = CAEN_DGTZ_SetDPPEventAggregation(handle, 1, 0);
 int dcoffset = config.read<int>("dcoffset");
   for (int i=0;i<8;i++) ret = CAEN_DGTZ_SetChannelDCOffset(handle, i, dcoffset);
  ret = CAEN_DGTZ_SetDPPAcquisitionMode(handle,CAEN_DGTZ_DPP_ACQ_MODE_Mixed , CAEN_DGTZ_DPP_SAVE_PARAM_EnergyAndTime);
 	int evagr = config.read<int>("evagr");
   for(i=0;i<8;i+=1)ret = CAEN_DGTZ_SetDPPEventAggregation(handle, evagr, i);
  for(i=0;i<8;i+=1)ret = CAEN_DGTZ_SetChannelPulsePolarity(handle, i,CAEN_DGTZ_PulsePolarityPositive);
 ret = CAEN_DGTZ_SetIOLevel(handle, CAEN_DGTZ_IOLevel_TTL);
 //ret = CAEN_DGTZ_SetChannelEnableMask(handle,0x15);
	ret = CAEN_DGTZ_SetDPP_VirtualProbe(handle, ANALOG_TRACE_1, CAEN_DGTZ_DPP_VIRTUALPROBE_Input);
	ret = CAEN_DGTZ_SetDPP_VirtualProbe(handle, ANALOG_TRACE_2, CAEN_DGTZ_DPP_VIRTUALPROBE_Baseline);
	ret = CAEN_DGTZ_SetDPP_VirtualProbe(handle, DIGITAL_TRACE_1, CAEN_DGTZ_DPP_DIGITALPROBE_Gate);
	ret = CAEN_DGTZ_SetDPP_VirtualProbe(handle, DIGITAL_TRACE_2, CAEN_DGTZ_DPP_DIGITALPROBE_GateShort);
	//ret = CAEN_DGTZ_SetDPPParameters(handle, 0xAA, &DPPParams);
   ret = CAEN_DGTZ_SetDPPParameters(handle, 0xFF, &DPPParams);
  if(ret != CAEN_DGTZ_Success) {
  printf("Errors during Digitizer Configuration.\n");
  //goto QuitProgram;
  }
   
  //}
  for(i=0;i<8;i+=1)ret=CAEN_DGTZ_SetDPPPreTriggerSize(handle,i,pretr_db[i]);
  //ret=CAEN_DGTZ_SetDPPPreTriggerSize(handle[0],1,10);
  uint32_t d32 = 0xFFFFFFFF;
  int triggerLogicBaseAddress=0x1084;
  uint32_t data,mask;
  data &= mask;
  d32 &= ~mask;
  d32 |= data;
  CAEN_DGTZ_ReadRegister(handle,0x8000,&d32);
  printf("CAEN 0x8000=%x\n\n",d32);
  //ret |= WriteRegisterBitmask(triggerLogicBaseAddress+256*i, 0x3, 0xFFFFFFFF);
  // ret = CAEN_DGTZ_WriteRegister(handle, 0x1084, d32);
  // ret = CAEN_DGTZ_WriteRegister(handle, 0x1084+256*2, d32);
  /*GENERIC_WRITE 0x1080 0x40000 0xC0000
GENERIC_WRITE 0x1180 0x40000 0xC0000

GENERIC_WRITE 0x1070 0xA 0xFF
GENERIC_WRITE 0x1170 0xA 0xFF

GENERIC_WRITE 0x106C 0x2 0xFF
GENERIC_WRITE 0x116C 0x2 0xFF

GENERIC_WRITE 0x1084 0x60 0xFF*/
   ret = CAEN_DGTZ_SetChannelSelfTrigger(handle,CAEN_DGTZ_TRGMODE_ACQ_ONLY,0x5);
  ret = CAEN_DGTZ_SetChannelEnableMask(handle, 0x15);
 //ret = CAEN_DGTZ_SetChannelSelfTrigger(handle[0],CAEN_DGTZ_TRGMODE_ACQ_ONLY,3);
 //CAEN_DGTZ_SetDPPTriggerMode (handle,CAEN_DGTZ_DPP_TriggerMode_Normal);
  CAEN_DGTZ_SetDPPTriggerMode (handle,CAEN_DGTZ_DPP_TriggerMode_Coincidence);
 /* Set trigger on channel 0 to be ACQ_ONLY */
    for(int i=0; i<8; i+=1){
      CAEN_DGTZ_TriggerMode_t mode;
      CAEN_DGTZ_DPP_TriggerMode_t mode2;
      CAEN_DGTZ_GetChannelSelfTrigger(handle, i, &mode);
      //cout<<"\tChannel "<<i<<"\tEnabled: "<<mode<<endl;
      printf("Selft %d-%d\n",i,mode);
      CAEN_DGTZ_GetDPPTriggerMode (handle,&mode2);
      printf("DPP mode =%d\n",mode2);
    }
  static int allocatedBufferMemory = 0;
  
  if(!allocatedBufferMemory){
    
    //uint32_t AllocatedSize;
    
    // Allocate buffer memory.  Seems to only need to happen once.
    int ret3 = CAEN_DGTZ_MallocReadoutBuffer(handle, &gBuffer,&AllocatedSize); /* WARNING: This malloc must be done after the digitizer programming */
    printf("allocated size=%li\n",AllocatedSize);
    if(ret3) {
      printf("Failed to malloc readout buffer\n");
    }
    
    allocatedBufferMemory = 1;
  }
  //uint32_t AllocatedSize;
  ret = CAEN_DGTZ_MallocDPPEvents(handle, (void**)Events, &AllocatedSize);
  ret = CAEN_DGTZ_MallocDPPWaveforms(handle, (void**)&Waveform, &AllocatedSize);   CAEN_DGTZ_ReadRegister(handle,0x8000,&d32);
  printf("CAEN 0x8000=%x\n\n",d32);
  sleep(3);
   

  
  
  return 1;
}


void do_exit(string OutFileName)
{
  // tree->Write();
  // file->Write();
  // if(NULL!=vme)  vme->closeVME();
  //  if(NULL!=tree)tree->Write();
  //  if(NULL!=file)file->Close();
  // Analyze histograms
  //
  //std::string filepath = dir + line;
  //const char* cfilepath = filepath.c_str();
  //  char *cfilepath_nonconst = new char[OutFileName.length() + 1];
  //  strcpy(cfilepath_nonconst, OutFileName.c_str());
  //  cout << "Analyze file:  " << cfilepath_nonconst << endl;
  // analyze(cfilepath_nonconst);
  //
  //signal(SIGTERM,SIG_DFL);
  //  signal(SIGINT ,SIG_DFL);
  std::cout<<"break"<<std::endl;
  //end=true;
  /*  CAEN_DGTZ_SWStopAcquisition(handle);
  printf("SWStopAcquisition\n");
  CAEN_DGTZ_CloseDigitizer(handle);
  printf("CloseDigitizer\n");
  CAEN_DGTZ_FreeReadoutBuffer(&gBuffer);
  CAEN_DGTZ_FreeDPPEvents(handle, (void**)Events);
  CAEN_DGTZ_FreeDPPWaveforms(handle, (void**)Waveform);*/
    //exit(0);
  end=true;
  printf("end=true\n");
  
};

void intHandler(int dummy)
{
  do_exit("c");

}
