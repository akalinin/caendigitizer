#ifndef __ANALYZE_H
#define __ANALYZE_H

#include "TH1.h" 
#include "TF1.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TFile.h"
#include "TPaveStats.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TPolyMarker.h"
#include "TSpectrum.h"
#include "TTree.h"
#include "TString.h"
#include "string.h"
#include <fstream>
#include <iostream>
//#include <filesystem>
#include <dirent.h>
//namespace fs = std::filesystem;
#include <sstream>

#include "analyze.h"

#include <fstream>
#include <iostream>
#include <dirent.h>
#include <sstream>



//#include "TF1.h"
//#include "TH1.h"
//#include "TH1F.h"
//#include "TROOT.h"
//#include "TTree.h"
//#include "TVector2.h"
//#include "TString.h"
//#include "TStyle.h"
//#include "TMath.h"
//#include "TFile.h"
//#include "TPaveStats.h"
//#include "TCanvas.h"
//#include "TLatex.h"
//#include "TPolyMarker.h"
//#include "TSpectrum.h"
//#include "TFile.h"


//#include "TString.h"
//#include "string.h"

TF1 *GP(TH1F* hist2proc, int xlow, int xhigh);
TF1 *LGFit(TH1F *hist);
TF1 *GFit(TH1F *hist);
//void analyze(char *filename);
//void analyze_multiple();
double generpoiss(double *x, double *p);
double landau_gauss(double *x, double *par);
//TF1 *langaufit(TH1F *his, Double_t *fitrange, Double_t *startvalues, Double_t *parlimitslo, //Double_t *parlimitshi, Double_t *fitparams, Double_t *fiterrors, Double_t *ChiSqr, Int_t *NDF);
//Double_t langaufun(Double_t *x, Double_t *par);
//TCanvas *params_stats(TH1F *hist2draw, TCanvas *se);

#endif
