#include <iostream>
#include <time.h>
#include <map>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include "TBox.h"
#include "TKey.h"
#include "TH1F.h"
#include "TF1.h"
#include "TProfile.h"
#include "THttpServer.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TList.h"
#include "TPaveStats.h"
#include "TPaveText.h"
#include <CAENDigitizer.h>
#include "ConfigFile.h"
#include "TSystem.h"
#include "TNamed.h"
#include <signal.h>
#include <bitset>
#include "Digi.h"
#include "analyze.h"
#include "w3.h"
using namespace std;

THttpServer *serv;
int NUMBER_OF_CHANNELS=4;
//TH1F *htrigger0,*htrigger2,*hsignal0,*hsignal2;
TH1F *htrigger[2],*hsignal[2];
TH1 *hsdata[2];
//TH1F *hsigtr0,*hbltr0,*hsigtr2,*hbltr2;
TH1F *hsigspectr[2],*hblspectr[2];
TH1F *hacsignal[2];//,*hacsignal2;
int intbl[2],intsig[2];
//int intbl2,intsig2;
//TH1F *hsig0,*hsig2;
TH1F *hcommon[2];
TH1F *hsignal0d1,*hsignal2d1;
TH1F *hsignal0d2,*hsignal2d2;
TH1F *htrev;
TBox *tpoly[2];//,*tpoly2;
//TLegend *tpave[2];
TPaveText *tpave[2];
//Bool_t bStart=kTRUE;
//Bool_t bStop=kFALSE;
map <int,double> parameters ;
//map <int,Bool_t> bStop;
//TCanvas *can1;
int rmin,rmax;
int windowwidth;
int handle;
char *gBuffer = NULL;
 TF1 *genp[2];
uint32_t buffsize;
uint32_t numEvents; 
int createfit=0;
bool runfitted=true;   

Bool_t myend=kFALSE;
Bool_t onrun=kFALSE;
int selft_db[8];
int thresh_db[8];
int dcoffset_db[8];
CAEN_DGTZ_TriggerMode_t ChannelTriggerMode[8];
int Version_used[8];
TNamed *pars;
int Nevents,Actime;
uint32_t samples;
int posttrigger;
char str[2000];
char *buffer;
char *EventPtr;
CAEN_DGTZ_EventInfo_t       EventInfo;
uint32_t AllocatedSize, BufferSize, NEvents;

CAEN_DGTZ_UINT16_EVENT_t    *Event16; 
CAEN_DGTZ_BoardInfo_t       BoardInfo;
int isVMEDevice, MajorNumber;
int NumEvents=1;
 uint16_t EnableMask;
CAEN_DGTZ_ErrorCode OrProgrammer();
CAEN_DGTZ_ErrorCode WriteRegisterBitmask(uint32_t address, uint32_t data, uint32_t mask);
int programm_digitizer(char *cfgfilename);
int poll_digitizer();
int read_digitizer();
void intHandler(int dummy);
void do_exit(string OutFileName);
void cmdStart();
void cmdStop();
void chNevents(int arg);
void chActime(int arg);
void chfithistos(int arg);
void chreadtemperatures(int arg);
void chthreshold(int arg1,int arg2);
void chdcoffset(int arg1,int arg2);
void chdcoffset2(int arg1);
void chsamples(int arg1);
void chposttrigger(int arg1);
void chwindow(int arg1,int arg2);
void chintwindow(int arg1);
void chintsig(int arg1,int arg2);
void ReadParameters(char *cfgfilename);
void UpdateParameters();
void serverconfig();
void resethistos();
void fithistos();
bool opendigitizer();
bool allocateevents();
int programdigitizer();
bool readdigitizer();
void createhistos();
void savefile(TString arg1);
int chtr[2]={-1,-1};
int chsig=3;
int readtemp=0;
TString temp0,temp1;



int main(int argc, char *argv[])

{
 signal(SIGINT, intHandler);

  //int Nevents = config.read<int>("nevents");
  //int Actime = config.read<int>("actime");
 char sname[200];
 
 int ch;
 int nevents=0;
 int actime=0;

 
 

 ReadParameters(argv[1]);
 createhistos();
 serverconfig();
 UpdateParameters();
 //sleep(1);
 opendigitizer();
 //UpdateParameters();
 serv->SetItemField("/Status", "value","Stop");
 serv->SetItemField("/Status", "_status", "0");
 while(!myend)
   {
     while(onrun)
       {
	 readdigitizer();
	 //char str[200];
	 sprintf(str,"Running\n%d/%d\n",nevent,Nevents);
	 serv->SetItemField("/Status", "value",str);
	 serv->SetItemField("/Status", "_status", "1");	 
	 serv->SetItemField("/Fits", "value", "Undefined");
	 if ((Nevents!=0)&&(nevent>Nevents)) cmdStop();
	 if((Actime!=0)&&((CurrentTime-StartTime)>Actime*1000)) {cout<<CurrentTime<<"  "<<StartTime<<endl;cmdStop();}
	 gSystem->ProcessEvents();
       }
     if ((!runfitted)&&(createfit!=0)) {runfitted=true;fithistos();}
     //  serv->ProcessRequests();
     gSystem->ProcessEvents();
   }
  CAEN_DGTZ_CloseDigitizer(handle);
  return 1;
}

void cmdStart(){
  //myend=kFALSE;
  PrevRateTime = util::get_time();
  StartTime=PrevRateTime;
  Nb=0;
  Ne=0;
  nCycles= 0;
  nevent=0;
  resethistos();
  programdigitizer();
  sleep(1);
  CAEN_DGTZ_SWStartAcquisition(handle);
  onrun=kTRUE;
  runfitted=false;
  std::cout<<"cmdStart()"<<std::endl;
  if (readtemp!=0)
    {
      wget(temp0, "http://minitrs01.cern.ch", "/", 80);
      serv->SetItemField("/Temperatures", "value", temp0);
    }
}

void cmdStop(){
  //myend=kTRUE;
  CAEN_DGTZ_SWStopAcquisition(handle);
  onrun=kFALSE;
   std::cout<<"cmdStop()"<<std::endl;
   serv->SetItemField("/Status", "value","Stop");
   serv->SetItemField("/Status", "value", "0");
   if (readtemp!=0)
     {
       wget(temp1, "http://minitrs01.cern.ch", "/", 80);
       serv->SetItemField("/Temperatures", "value", temp0+"\n"+temp1);
     }
}
void chNevents(int arg){
  Nevents=arg;
  printf("Nevents changed to %d\n",arg);
  UpdateParameters();
}

void chActime(int arg){
  Actime=arg;
  printf("Actime changed to %d\n",arg);
  UpdateParameters();
}

void chfithistos(int arg){
  createfit=arg;
  printf("createfit changed to %d\n",arg);
  UpdateParameters();
}

void chreadtemperatures(int arg){
  readtemp=arg;
  printf("readtemp changed to %d\n",arg);
  UpdateParameters();
}


void chsamples(int arg1){
  samples=arg1;
  printf("samples changed to %d\n",arg1);
  UpdateParameters();
}

void chposttrigger(int arg){
  posttrigger=arg;
  printf("posttrigger changed to %d\n",arg);
  UpdateParameters();
}

void chthreshold(int arg1,int arg2){
 
  thresh_db[chtr[0]]=arg1;
  thresh_db[chtr[1]]=arg2;
  printf("threshold for ch %d changed to %d\n",chtr[0],arg1);
  printf("threshold for ch %d changed to %d\n",chtr[1],arg2);
  UpdateParameters();
}

void chwindow(int arg1,int arg2){
 
  rmin=arg1;
  rmax=arg2;
  printf("window changed(%d,%d)\n",arg1,arg2);

  UpdateParameters();
}

void chintwindow(int arg1){
 
  windowwidth=arg1;
  printf("window width changed %d)\n",arg1);

  UpdateParameters();
}

void chintsig(int arg1,int arg2){
 
  intsig[0]=arg1;
  intsig[1]=arg2;
  printf("integration window for signal(triggerch%d) changed to (%d,%d)\n",chtr[0],arg1,arg1+windowwidth);
  printf("integration window for signal(triggerch%d) changed to (%d,%d)\n",chtr[1],arg2,arg2+windowwidth);
  UpdateParameters();
}

void chdcoffset(int arg1,int arg2){
 
  dcoffset_db[chtr[0]]=arg1;
  dcoffset_db[chtr[1]]=arg2;
  printf("dcoffset for ch %d changed to %d\n",chtr[0],arg1);
  printf("dcoffset for ch %d changed to %d\n",chtr[1],arg2);
  UpdateParameters();
}
void chdcoffset2(int arg1){
 
  dcoffset_db[chsig]=arg1;
  ///dcoffset_db[2]=arg2;
  printf("dcoffset for signal ch %d changed to %d\n",chsig,arg1);
  //printf("dcoffset for ch 2 changed to %d\n",arg2);
  UpdateParameters();
}
void resethistos()
{
  for(int i=0;i<2;i++)
    {
      htrigger[i]->Reset();
      hsignal[i]->Reset();
      hsdata[i]->Reset();
      
      hsigspectr[i]->Reset();
      hblspectr[i]->Reset();
      hacsignal[i]->Reset();
      
      hcommon[i]->Reset();
      
    }
  htrev->Reset();
}

void ReadParameters(char *cfgfilename)
{

 TString eval;
 int counter;
 ConfigFile config(cfgfilename);
  Nevents = config.read<int>("nevents");
  Actime = config.read<int>("actime");
 

 TString selft = config.read<string>("selft").data();
 
  selft +=","; //needed for seperation of channels
 
  eval="";
  counter= 0;
  for (int i=0; i< selft.Length(); i++){
    if(selft[i]!=',')				// if the element isn't a comma,
      eval.Append(selft[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	selft_db[counter]=element;
	counter ++;// append the integer to the array
      }	
  }
   for (int i=0;i<8;i++) 
    if ((selft_db[i]==1)&&(chtr[0]==-1)) chtr[0]=i;
    else									
      if ((selft_db[i]==1)&&(chtr[0]!=-1)) chtr[1]=i;
  printf("trigger %d OR %d\n",chtr[0],chtr[1]);


 TString thresh = config.read<string>("thresh").data();
  
  thresh +=","; //needed for seperation of channels
 
 
   counter= 0;
  for (int i=0; i< thresh.Length(); i++){
    if(thresh[i]!=',')				// if the element isn't a comma,
      eval.Append(thresh[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	thresh_db[counter]=element;
	counter ++;// append the integer to the array
      }	
      }

 TString dcoffset = config.read<string>("dcoffset").data();
  
  dcoffset +=","; //needed for seperation of channels
 
 
   counter= 0;
  for (int i=0; i< dcoffset.Length(); i++){
    if(dcoffset[i]!=',')				// if the element isn't a comma,
      eval.Append(dcoffset[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	dcoffset_db[counter]=element;
	counter ++;// append the integer to the array
      }	
      }

  windowwidth = config.read<int>("windowwidth");
  createfit = config.read<int>("fit");
  readtemp = config.read<int>("readtemp");

 TString intsigst = config.read<string>("intsig").data();
  
  intsigst +=","; //needed for seperation of channels
 
 
   counter= 0;
  for (int i=0; i< intsigst.Length(); i++){
    if(intsigst[i]!=',')				// if the element isn't a comma,
      eval.Append(intsigst[i]);		// append it to the string.
    else						// otherwise,
      {	int element = eval.Atoi();	// convert the string to an integer,
	eval = "";				// clear the string, an
	intsig[counter]=element;
	intbl[counter]=intsig[counter]-windowwidth;
	counter ++;// append the integer to the array
      }	
      }
  samples = config.read<int>("samples");
  posttrigger = config.read<int>("posttrigger");
  rmin = config.read<int>("rmin");
  rmax = config.read<int>("rmax");
}

void UpdateParameters(){
  sprintf(str,"Run will be for %d events or for %d seconds time",Nevents,Actime);
  sprintf(str,"%s\nNumber of samples %d",str,samples);
  sprintf(str,"%s\nposttrigger = %d% ",str,posttrigger);
  sprintf (str,"%s\ntriggered channel ",str);

  for (int i=0;i<8;i++) if (selft_db[i]==1) sprintf(str,"%s %d OR",str,i);
  sprintf (str,"%s\nthresholds:\n",str);
  for (int i=0;i<8;i++) if (selft_db[i]==1) sprintf(str,"%schannel %d = %d\n",str,i,thresh_db[i]);
  sprintf (str,"%s\ndcoffset trigger:\n",str);
  for (int i=0;i<8;i++) if (selft_db[i]==1) sprintf(str,"%schannel %d = %d\n",str,i,dcoffset_db[i]);

  sprintf (str,"%s\ndcoffset signal ch %d=%d\n",str,chsig,dcoffset_db[chsig]);
  sprintf (str,"%s\nintegration window width = %d\n",str,windowwidth);
  for (int i=0;i<2;i++) sprintf(str,"%sintegration window trigger %d = (%d,%d)\n",str,chtr[i],intsig[i],intsig[i]+windowwidth);
  sprintf (str,"%swindow = ( %d , %d )\n",str,rmin,rmax);
  sprintf (str,"%screatefit = %d\n",str,createfit);
  sprintf (str,"%sreadtemperatures = %d\n",str,readtemp);
  serv->SetItemField("/Parameters", "value",str);
  createhistos();
  serverconfig();
}

bool opendigitizer()
{
 CAEN_DGTZ_ErrorCode ret = CAEN_DGTZ_Success;
 ret = CAEN_DGTZ_OpenDigitizer(CAEN_DGTZ_USB, 0, 0, 0, &handle);
 if (ret) {
   cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
   //if(fman.isInit())
   // fman.DeleteDir();
   return false;
  }
  ret = CAEN_DGTZ_GetInfo(handle, &BoardInfo);
  if (ret) {
    cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
    return false;
      
  }
  printf("Connected to CAEN Digitizer Model %s\n", BoardInfo.ModelName);
  printf("ROC FPGA Release is %s\n", BoardInfo.ROC_FirmwareRel);
  printf("AMC FPGA Release is %s\n", BoardInfo.AMC_FirmwareRel);

  // Check firmware rivision (DPP firmwares cannot be used with WaveDump */
  sscanf(BoardInfo.AMC_FirmwareRel, "%d", &MajorNumber);
  if (MajorNumber >= 128) {
    printf("This digitizer has a DPP firmware\n");
    cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
    return false;
  }

  int ret2 = programdigitizer();
  if (ret2) {
    return false;
  }
  return true;
}
int programdigitizer()
{
  buffer = NULL;
  EventPtr = NULL;
  
  Event16=NULL; /* generic event struct with 16 bit data (10, 12, 14 and 16 bit digitizers */
  int i,ret = 0;
  ret |= CAEN_DGTZ_Reset(handle);
  if (ret != 0) {
    printf("Error: Unable to reset digitizer.\nPlease reset digitizer manually then restart the program\n");
    cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
    return ret;
  }
  ret |= CAEN_DGTZ_SetRecordLength(handle, samples);
  ret |= CAEN_DGTZ_GetRecordLength(handle, &samples);
  ret |= CAEN_DGTZ_SetPostTriggerSize(handle, posttrigger);

  ret |= CAEN_DGTZ_SetMaxNumEventsBLT(handle, NumEvents);
  ret |= CAEN_DGTZ_SetAcquisitionMode(handle, CAEN_DGTZ_SW_CONTROLLED);
  ret |= CAEN_DGTZ_SetExtTriggerInputMode(handle, CAEN_DGTZ_TRGMODE_ACQ_ONLY);

 uint16_t tempEnableMask=0;
 EnableMask=1;
  bool trigChannel=false;
  
  for(int i=0; i<NUMBER_OF_CHANNELS; i++){
    
    
    if((i==chtr[0])||(i==chtr[1])||(i==chsig)){
     
      tempEnableMask += (1<<i);
    }else{
      thresh_db[i]=0;
      continue;
    }
   


    if((i==chtr[0])||(i==chtr[1])){
      trigChannel=true;
      ChannelTriggerMode[i]=CAEN_DGTZ_TRGMODE_ACQ_AND_EXTOUT;//CAEN_DGTZ_TRGMODE_ACQ_ONLY;
      Version_used[i]=1;
    } else {
      ChannelTriggerMode[i]=CAEN_DGTZ_TRGMODE_DISABLED;
      Version_used[i]=0;
      }
  }

  if(tempEnableMask!=0)
    EnableMask=tempEnableMask;
  bitset<8> mask(EnableMask);
  cout<<"EnableMask \t\t"<<mask <<endl;


  ret |= CAEN_DGTZ_SetChannelEnableMask(handle, EnableMask);
  for (i = 0; i < NUMBER_OF_CHANNELS; i++) {
    if (EnableMask & (1<<i)) {
      ret |= CAEN_DGTZ_SetChannelDCOffset(handle, i, dcoffset_db[i]);
      ret |= CAEN_DGTZ_SetChannelTriggerThreshold(handle, i, thresh_db[i]);
      //   if (i!=0)ret |= CAEN_DGTZ_SetTriggerPolarity(handle, i,CAEN_DGTZ_TriggerOnRisingEdge );
      //    else //.TriggerEdge
          ret |= CAEN_DGTZ_SetTriggerPolarity(handle, i,CAEN_DGTZ_TriggerOnFallingEdge );
    }
  }
  ret |= CAENDGTZ_API CAEN_DGTZ_SetChannelPulsePolarity(handle,0,CAEN_DGTZ_PulsePolarityNegative );
  ret |= CAENDGTZ_API CAEN_DGTZ_SetChannelPulsePolarity(handle,3,CAEN_DGTZ_PulsePolarityNegative );
    ret |= OrProgrammer();
  if (ret){
    printf("Warning: errors found during the programming of the digitizer.\nSome settings may not be executed\n");
    cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
  }
  ret |= allocateevents();
  return ret;
}

CAEN_DGTZ_ErrorCode OrProgrammer()
{
  int ret=0;
  
  int triggerLogicBaseAddress=0x1084;
  
  // channel pair settings for x730 boards
  for (int i = 0; i < NUMBER_OF_CHANNELS; i += 2) {
    if (EnableMask & (0x3 << i)) {
      CAEN_DGTZ_TriggerMode_t mode = ChannelTriggerMode[i];
      uint32_t pair_chmask = 0;
	
      // Build mode and relevant channelmask. The behaviour is that,
      // if the triggermode of one channel of the pair is DISABLED,
      // this channel doesn't take part to the trigger generation.
      // Otherwise, if both are different from DISABLED, the one of
      // the even channel is used.

      
      if (ChannelTriggerMode[i] != CAEN_DGTZ_TRGMODE_DISABLED && ChannelTriggerMode[i + 1] != CAEN_DGTZ_TRGMODE_DISABLED) {
	//channel i and i+1 are both enabled
	cout<<"Channel "<<i<<" and "<<i+1<<" enableddd\n";
	ret |= WriteRegisterBitmask(triggerLogicBaseAddress+256*i, 0x3, 0xFFFFFFFF);
	pair_chmask = (0x3 << i);
	
      } else if(ChannelTriggerMode[i] != CAEN_DGTZ_TRGMODE_DISABLED && ChannelTriggerMode[i + 1] == CAEN_DGTZ_TRGMODE_DISABLED) {
	//channel i enabled, channel i+1 disabled
	 cout<<"Channel "<<i<<" enabled and "<<i+1<<" disabled\n";
	pair_chmask = (0x1 << i); 
	ret |= WriteRegisterBitmask(triggerLogicBaseAddress+256*i, 0x1, 0xFFFFFFFF);
	
      }	else if(ChannelTriggerMode[i] == CAEN_DGTZ_TRGMODE_DISABLED&&ChannelTriggerMode[i+1] != CAEN_DGTZ_TRGMODE_DISABLED){
	//channel i disabled, channel i+1 enabled
	 cout<<"Channel "<<i<<" disabled and "<<i+1<<" enabled\n";
	ret |= WriteRegisterBitmask(triggerLogicBaseAddress+256*i, 0x2, 0xFFFFFFFF);
	mode = ChannelTriggerMode[i + 1];
	pair_chmask = (0x2 << i);
      }else{
	 cout<<"Channel "<<i<<" and "<<i+1<<" disabled\n";
      }
      
      
          
      
      pair_chmask &= EnableMask;
      ret |= CAEN_DGTZ_SetChannelSelfTrigger(handle, mode, pair_chmask);
         
    }
  }
  
  return (CAEN_DGTZ_ErrorCode)ret;

}


CAEN_DGTZ_ErrorCode WriteRegisterBitmask(uint32_t address, uint32_t data, uint32_t mask)
{
  int32_t ret = CAEN_DGTZ_Success;
  uint32_t d32 = 0xFFFFFFFF;

  ret = CAEN_DGTZ_ReadRegister(handle, address, &d32);
  if(ret != CAEN_DGTZ_Success){
    cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
    return (CAEN_DGTZ_ErrorCode)ret;
  }

  data &= mask;
  d32 &= ~mask;
  d32 |= data;
  ret = CAEN_DGTZ_WriteRegister(handle, address, d32);
  
  return (CAEN_DGTZ_ErrorCode)ret;
}

bool allocateevents()
{  
  int ret;
  ret = CAEN_DGTZ_AllocateEvent(handle, (void**)&Event16);
  
  if (ret != CAEN_DGTZ_Success) {
    cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
    return false;
  }
  ret = CAEN_DGTZ_MallocReadoutBuffer(handle, &buffer,&AllocatedSize); /* WARNING: This malloc must be done after the digitizer programming */
  if (ret) {
    cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
    return false;
  }
  return true;
}

bool readdigitizer()
{
  int ret;
    ret = CAEN_DGTZ_ReadData(handle, CAEN_DGTZ_SLAVE_TERMINATED_READOUT_MBLT, buffer, &BufferSize);
    if (ret) {
      cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
      exit(0);
    }
    NEvents = 0;
    if (BufferSize != 0) {
      ret = CAEN_DGTZ_GetNumEvents(handle, buffer, BufferSize, &NEvents);
      if (ret) {
	cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
	exit(0);
      }
    }
    else {
      uint32_t lstatus;
      ret = CAEN_DGTZ_ReadRegister(handle, CAEN_DGTZ_ACQ_STATUS_ADD, &lstatus);
      if (ret) {
	printf("Warning: Failure reading reg:%x (%d)\n", CAEN_DGTZ_ACQ_STATUS_ADD, ret);
	cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
      }
      else {
	if (lstatus & (0x1 << 19)) {
	  exit(0);
	}
      }
    }
  /* Calculate throughput and trigger rate (every second) */
    Nb += BufferSize;
    Ne += NEvents;
    nevent +=NEvents;
    CurrentTime = util::get_time();
    ElapsedTime = CurrentTime - PrevRateTime;

    nCycles++;
    if (ElapsedTime > 1000) {
      if (Nb == 0)
	if (ret == CAEN_DGTZ_Timeout) printf ("Timeout...\n"); else printf("No data...\n");
      else
      
	  printf("Reading at %.2f MB/s (Trg Rate: %.2f Hz, %d events)\n", (float)Nb/((float)ElapsedTime*1048.576f), (float)Ne*1000.0f/(float)ElapsedTime, nevent);
      nCycles= 0;
      Nb = 0;
      Ne = 0;
      PrevRateTime = CurrentTime;
    }	

 /* Analyze data */

   for(int j = 0; j < (int)NEvents; j++) 
     {
     {

      /* Get one event from the readout buffer */
      ret = CAEN_DGTZ_GetEventInfo(handle, buffer, BufferSize, j, &EventInfo, &EventPtr);
      //cout<<ret<<endl;
      if (ret) {
	cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
	continue;
	//exit(0);
      }
      /* decode the event */
      ret = CAEN_DGTZ_DecodeEvent(handle, EventPtr, (void**)&Event16);

      if (ret) {
	cout<<errors[abs((int)ret)]<<" (code "<<ret<<")"<<endl;
	exit(0);
      }


   }
     int trev=-1; int bl_=0,sig_=0,sumdata=0;
     int maximumvalue[2]={0,0};
     for(int i=int(samples*(100-posttrigger)/100.); i>5; i--){
       if ((maximumvalue[0]==0)&&(Event16->DataChannel[chtr[0]][i]>int(thresh_db[chtr[0]]))) maximumvalue[0]=1;
       if ((maximumvalue[1]==0)&&(Event16->DataChannel[chtr[1]][i]>int(thresh_db[chtr[1]]))) maximumvalue[1]=1;
     }
   for(int i=int(samples*(100-posttrigger)/100.); i>5; i--)
     {
         //if((int(-Event16->DataChannel[chtr[1]][i])+int(Event16->DataChannel[chtr[1]][i-5]))>int(.2*thresh_db[chtr[1]])) {trev=1;break;}
       if ((maximumvalue[0]==1)&&(int(Event16->DataChannel[chtr[0]][i])<int(thresh_db[chtr[0]]))) {trev=0;break;}
       if ((maximumvalue[1]==1)&&(int(Event16->DataChannel[chtr[1]][i])<int(thresh_db[chtr[1]]))) {trev=1;break;}
	 /*  if((int(-Event16->DataChannel[chtr[0]][i])+int(Event16->DataChannel[chtr[0]][i-5]))>int(.2*thresh_db[chtr[0]])) {trev=0;break;}*/
       
     }
   //trev=1;
   uint16_t sdata[samples];
   for (int ui=0;ui<samples;ui++) sdata[ui]= 16000-Event16->DataChannel[chsig][ui];
   for(int ch=0; ch<NUMBER_OF_CHANNELS; ch++){

     if (EnableMask & (1<<ch)){
       
       int Size=Event16->ChSize[ch];
       if(Size<=0)
	 continue;
    
       for(int i=0; i<Size; i++){
        if (ch==chtr[0]) htrigger[0]->SetBinContent(i+1,Event16->DataChannel[ch][i]);
	if (ch==chtr[1]) htrigger[1]->SetBinContent(i+1,Event16->DataChannel[ch][i]);

	if((ch==chsig)&&(trev!=-1)){
	  /*
	hsignal[trev]->SetBinContent(i+1,Event16->DataChannel[ch][i]);
	if (i>4&&i<1000)
	  {
	    sumdata=Event16->DataChannel[ch][i]*8+Event16->DataChannel[ch][i-1]*4+Event16->DataChannel[ch][i+1]*4+Event16->DataChannel[ch][i-2]*2+Event16->DataChannel[ch][i+2]*2+Event16->DataChannel[ch][i-3]+Event16->DataChannel[ch][i-3];
	    sumdata=(Event16->DataChannel[ch][i]+Event16->DataChannel[ch][i-1]+Event16->DataChannel[ch][i+1]+Event16->DataChannel[ch][i-2]+Event16->DataChannel[ch][i+2]+Event16->DataChannel[ch][i-3]+Event16->DataChannel[ch][i+3]);
	    Double_t Mmin=1000;
	    for(int mini=-3;mini<4;mini++){Mmin=TMath::Min(Mmin,TMath::Abs(Event16->DataChannel[ch][i+mini]-sumdata/7.0));}
	    hsdata[trev]->SetBinContent(i,(sumdata/7.0*(Mmin<1))+Event16->DataChannel[ch][i]*(Mmin>=1));
	  }

	if((i>intbl[trev])&&(i<intbl[trev]+windowwidth)) bl_+=Event16->DataChannel[ch][i];
	if((i>intsig[trev])&&(i<intsig[trev]+windowwidth)) sig_+=Event16->DataChannel[ch][i];*/
	hsignal[trev]->SetBinContent(i+1,sdata[i]);
	if((i>intbl[trev])&&(i<intbl[trev]+windowwidth)) bl_+=sdata[i];
	if((i>intsig[trev])&&(i<intsig[trev]+windowwidth)) sig_+=sdata[i];

	}
       }
       //cout<<ch<<endl;
     }
   }
   if (trev!=-1) 
     {
       //TSpectrum *s = new TSpectrum(2*1);
       //hsdata[trev]=s->Background(hsignal[trev],20,"same");
       hacsignal[trev]->Add(hsignal[trev]);
       hblspectr[trev]->Fill(bl_);
       hsigspectr[trev]->Fill(sig_);
       hcommon[trev]->Fill((sig_-bl_));
       
     }
   htrev->Fill(trev);
     }

}


void createhistos()
{
  char sname[200],stitle[200];
  for (int i=0;i<2;i++)
    {
      sprintf(sname,"trigwf_%d",chtr[i]);
      sprintf(stitle,"trigger_waveform_ch%d",chtr[i]);
      if (htrigger[i]) htrigger[i]->Delete();
      htrigger[i]=new TH1F(sname,stitle,samples,0,samples);
      htrigger[i]->GetXaxis()->SetTitle("4ns");
      sprintf(sname,"signalwf_%d",chtr[i]);
      sprintf(stitle,"signal_waveform_trigger_ch%d",chtr[i]);
      if (hsignal[i]) hsignal[i]->Delete();
      hsignal[i]=new TH1F(sname,stitle,samples,0,samples);
      hsignal[i]->GetXaxis()->SetTitle("4ns");

      sprintf(sname,"signaldata_%d",chtr[i]);
      sprintf(stitle,"signal_data_trigger_ch%d",chtr[i]);
      if (hsdata[i]) hsdata[i]->Delete();
      hsdata[i]=new TH1F(sname,stitle,samples,0,samples);


      sprintf(sname,"acsignalwf_%d",chtr[i]);
      sprintf(stitle,"accum_signal_waveform_trigger_ch%d",chtr[i]);
      if (hacsignal[i]) hacsignal[i]->Delete();
      hacsignal[i]=new TH1F(sname,stitle,samples,0,samples);
      hacsignal[i]->GetXaxis()->SetTitle("4ns");
      sprintf(sname,"signal_spectr_%d",chtr[i]);
      sprintf(stitle,"signal_spectrum_trigger_ch%d",chtr[i]);
      if (hsigspectr[i]) hsigspectr[i]->Delete();
      hsigspectr[i]=new TH1F(sname,stitle,1000,rmin,rmax);
      hsigspectr[i]->SetLineColor(5+i);
      sprintf(sname,"bl_spectr_%d",chtr[i]);
      sprintf(stitle,"baseline_spectrum_trigger_ch%d",chtr[i]);
      if (hblspectr[i]) hblspectr[i]->Delete();
      hblspectr[i]=new TH1F(sname,stitle,1000,rmin,rmax);
      hblspectr[i]->SetLineColor(3+i);
      sprintf(sname,"common_spectr_%d",chtr[i]);
      sprintf(stitle,"common_spectrum_trigger_ch%d",chtr[i]);
      if (hcommon[i]) hcommon[i]->Delete();
      //   hcommon[i]=new TH1F(sname,stitle,(rmax-rmin+300)/(4+(1-i)*4),-300,(rmax-rmin));
      if (i==0) hcommon[i]=new TH1F(sname,stitle,(rmax-rmin+300),-300,(rmax-rmin));
      if (i!=0) hcommon[i]=new TH1F(sname,stitle,(rmax-rmin+300)/2,-300,(rmax-rmin));
      if (tpoly[i]) tpoly[i]->Delete();
      tpoly[i]=new TBox(intsig[i],0,intsig[i]+windowwidth,17000);
      tpoly[i]->SetFillColorAlpha(kRed,0.8);
    
	sprintf(sname,"pave_%d",chtr[i]);
	/*	   if(!tpave[i]) {
	tpave[i]= new TPaveText();
	tpave[i]->SetName(sname);
	//tpave[i]->SetTextSize(0.025);
	//tpave[i]->SetTextAlign(13);  //align at top
	//tpave[i]->DrawLatex(.2,.9,"K_{S}");
	//tpave[i]->DrawLatex(.3,.9,"K^{*0}");
	tpave[i]->SetX1NDC(0.);
	tpave[i]->SetX2NDC(1.);
	tpave[i]->SetY1NDC(0);
	tpave[i]->SetY2NDC(1.);
	tpave[i]->SetOption("");
	tpave[i]->SetFillColorAlpha(kRed,0.8);
	//if(hcommon[i])hcommon[i]->GetListOfFunctions()->Add(tpave[i]);
	
	//	tpave[i]->AddText("test");
	}*/


    }
  if (htrev) htrev->Delete();
 htrev=new TH1F("trev","trev",4,-1,3);
 


}

void intHandler(int dummy)


{
  do_exit("c");

}

void do_exit(string OutFileName)
{
  std::cout<<"break"<<std::endl;
  //CAEN_DGTZ_SWStopAcquisition(handle);
  std::cout<<"SWStop"<<std::endl;
  onrun=kFALSE;
  std::cout<<"cmdStop()"<<std::endl;
  //cmdStop();
  //myend=kTRUE;
  printf("end=true\n");
  sleep(1);
  //CAEN_DGTZ_CloseDigitizer(handle);
  myend=kTRUE;
};

void serverconfig()
{
   
  gInterpreter->Declare("void cmdStart();"); 
  gInterpreter->Declare("void cmdStop();");
  gInterpreter->Declare("void chNevents(int arg);");
  gInterpreter->Declare("void chActime(int arg);");
  gInterpreter->Declare("void chfithistos(int arg);");
  gInterpreter->Declare("void chreadtemperatures(int arg);");
  gInterpreter->Declare("void chsamples(int arg1);");
  gInterpreter->Declare("void chposttrigger(int arg);");
  gInterpreter->Declare("void chthreshold(int arg1,int arg2);");
  gInterpreter->Declare("void chdcoffset(int arg1,int arg2);");
  gInterpreter->Declare("void chwindow(int arg1,int arg2);");
  gInterpreter->Declare("void chintwindow(int arg1);");
  gInterpreter->Declare("void chintsig(int arg1,int arg2);");
  gInterpreter->Declare("void chdcoffset2(int arg1);");
  gInterpreter->Declare("int programdigitizer();");
  gInterpreter->Declare("void resethistos();");
  gInterpreter->Declare("void fithistos();");
  gInterpreter->Declare("void savefile(TString arg1);");
  if(!serv){
  serv=new THttpServer("http:9000;rw");

 
 

 serv->RegisterCommand("/commands/Start", "cmdStart();", "button;rootsys/icons/ed_execute.png");
 serv->RegisterCommand("/commands/Stop", "cmdStop();", "button;rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/FitAfterRun", "chfithistos(%arg1%);", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/ReadTemperatures", "chreadtemperatures(%arg1%);", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/RunTime", "chActime(%arg1%);", "rootsys/icons/ed_interrupt.png");

 serv->RegisterCommand("/commands/Program_digitizer", "programdigitizer();", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Change_number_of_events", "chNevents(%arg1%);cout<<%arg1%<<endl;", "rootsys/icons/ed_interrupt.png");

 serv->RegisterCommand("/commands/Change_samples", "chsamples(%arg1%);cout<<%arg1%<<endl;", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Change_posttrigger", "chposttrigger(%arg1%);cout<<%arg1%<<endl;", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Change_thresholds", "chthreshold(%arg1%,%arg2%);", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Change_dcoffset_trigger", "chdcoffset(%arg1%,%arg2%);", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Change_dcoffset_signal", "chdcoffset2(%arg1%);", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Change_window", "chwindow(%arg1%,%arg2%);", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Change_integration_width", "chintwindow(%arg1%);", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Change_integration_starting_point", "chintsig(%arg1%,%arg2%);", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Reset_histos", "resethistos();", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Fit_histos", "fithistos();", "rootsys/icons/ed_interrupt.png");
 serv->RegisterCommand("/commands/Save2file", "savefile(\"%arg1%\");", "button;rootsys/icons/bld_save.png");
 serv->CreateItem("/Parameters","parameters output");
 serv->SetItemField("/Parameters", "_kind", "Text");
 serv->CreateItem("/Status","status output");
 serv->SetItemField("/Status", "_kind", "Text");
 serv->SetItemField("/Status", "_size", "24");
 serv->SetItemField("/Status", "_font", "24");
 serv->SetItemField("/Status", "value", "0");
 serv->CreateItem("/Fits","fits output");
 serv->SetItemField("/Fits", "_kind", "Text");
 serv->CreateItem("/Temperatures","Temperatures output");
 serv->SetItemField("/Temperatures", "_kind", "Text");

  }
 
 char sname[400];
 for (int i=0;i<2;i++)
   {
     sprintf(sname,"/trigger_ch%d",chtr[i]);
     serv->Register(sname,htrigger[i]);
     serv->Register(sname,hsignal[i]);
     serv->Register(sname,hacsignal[i]);
     serv->Register(sname,hcommon[i]);
     serv->Register(sname,hblspectr[i]);
     serv->Register(sname,hsigspectr[i]);
     serv->Register(sname,hsdata[i]);
     serv->Register(sname,tpoly[i]);
     //   serv->Register(sname,tpave[i]);

   }
 //serv->SetItemField("/", "_layout", "grid2x2");
 serv->SetItemField("/", "_layout", "vert2222_8441");
 serv->SetItemField("/","_monitoring","10");
 //sprintf(sname,"[trigger_ch%d/common_spectr_%d,trigger_ch%d/common_spectr_%d,trigger_ch%d/pave_%d,trigger_ch%d/pave_%d,Parameters,trigger_ch%d/bl_spectr_%dtrigger_ch%d/signalwf_%d+trigger_ch%d/TBox,trigger_ch%d/signalwf_%d+trigger_ch%d/TBox]",chtr[0],chtr[0],chtr[1],chtr[1],chtr[0],chtr[0],chtr[1],chtr[1],chtr[0],chtr[0],chtr[0],chtr[1],chtr[1],chtr[1]);
 // sprintf(sname,"[trigwf_%d]",chtr[0]);
 sprintf(sname,"[trigger_ch%d/common_spectr_%d,trigger_ch%d/common_spectr_%d,Parameters,trigger_ch%d/bl_spectr_%d+trigger_ch%d/bl_spectr_%d+trigger_ch%d/signal_spectr_%d+trigger_ch%d/signal_spectr_%d,trigger_ch%d/signalwf_%d+trigger_ch%d/TBox,trigger_ch%d/signalwf_%d+trigger_ch%d/TBox,Status,Temperatures]",chtr[0],chtr[0],chtr[1],chtr[1],chtr[0],chtr[0],chtr[1],chtr[1],chtr[0],chtr[0],chtr[1],chtr[1],chtr[0],chtr[0],chtr[0],chtr[1],chtr[1],chtr[1]);
 printf("%s\n",sname);
 serv->SetItemField("/","_drawitem",sname);
}

void fithistos()
{
 TF1 *gaus0, *gaus1, *gp;
 char stat[100],sname[100];
 // TH1F *htemp=(TH1F*)hcommon[0]->Clone();
 //TF1 *gtemp=LGFit(*htemp);
TF1 *gtemp = LGFit(hcommon[0]);
sprintf(sname,"pave%d",chtr[0]);
      TPaveText *pave =new TPaveText();
	pave->SetName(sname);
		pave->SetX1NDC(0.5);
		pave->SetX2NDC(.9);
		pave->SetY1NDC(0.5);
		pave->SetY2NDC(.9);
	pave->SetOption("");
	//	pave->AddText("test");

  if (pave)
    {
   pave->Clear();

  pave->AddText("LG parameters:");
  sprintf(str,"LG parameters:");
sprintf(stat, "Par_{0} = %5.2f #pm %3.2f", gtemp->GetParameter(0), gtemp->GetParError(0));pave->AddText(stat);
 sprintf(str,"%s %s\n",str,stat);
sprintf(stat, "Par_{1} = %5.2f #pm %3.2f", gtemp->GetParameter(1), gtemp->GetParError(1));pave->AddText(stat);
//sprintf(str,"%s\n stat");
 sprintf(str,"%s %s\n",str,stat);
sprintf(stat, "Par_{2} = %5.2f #pm %3.2f", gtemp->GetParameter(2), gtemp->GetParError(2));pave->AddText(stat);
//sprintf(str,"%s\n stat");
 sprintf(str,"%s %s\n",str,stat);
  sprintf(stat, "Par_{3} = %5.2f #pm %3.2f", gtemp->GetParameter(3), gtemp->GetParError(3));
  //sprintf(str,"%s\n stat");
 sprintf(str,"%s %s\n",str,stat);
      pave->AddText(stat);
  hcommon[0]->GetListOfFunctions()->Add(pave);
    }
  for(int i=1;i<2;i++){
    if (hcommon[i]->GetEntries()<1000) continue;
  genp[i]=GP(hcommon[i],-60,5000);
  // TPaveStats *ps = (TPaveStats*)hcommon[i]->GetListOfFunctions()->FindObject("stats");
  //printf("ps....");
  sprintf(sname,"pave%d",chtr[i]);
      TPaveText *pave =new TPaveText();
	pave->SetName(sname);
		pave->SetX1NDC(0.5);
		pave->SetX2NDC(.9);
		pave->SetY1NDC(0.5);
		pave->SetY2NDC(.9);
	pave->SetOption("");
	//	pave->AddText("test");

  if (pave)
    { 
      // printf("found\n");
      // tpave[i]->Clear();
      // TList *listOfLines = tpave[i]->GetListOfLines();
  
      pave->Clear();
      pave->AddText("GP parameters:");
      sprintf(str,"%s\n GP parameters:", str);
      sprintf(stat, "#chi^{2} / ndf = %4.2f / %4.2d", genp[i]->GetChisquare(), genp[i]->GetNDF());
      pave->AddText(stat);
      //sprintf(str,"%s\n stat");
 sprintf(str,"%s %s\n",str,stat);
      sprintf(stat, "Gain_{SPE} = %4.2f #pm %3.2f", genp[i]->GetParameter(1), genp[i]->GetParError(1));
      pave->AddText(stat);
      //sprintf(str,"%s\n stat");
 sprintf(str,"%s %s\n",str,stat);
      sprintf(stat, "#lambda = %4.2f #pm %3.2f", genp[i]->GetParameter(0), genp[i]->GetParError(0));
      pave->AddText(stat);
      //sprintf(str,"%s\n stat");
 sprintf(str,"%s %s\n",str,stat);
      sprintf(stat, "Gain_{SPE} = %4.2f #pm %3.2f", genp[i]->GetParameter(1), genp[i]->GetParError(1));
      pave->AddText(stat);
      sprintf(stat, "Mean_{0} = %5.2f #pm %3.2f", genp[i]->GetParameter(4), genp[i]->GetParError(4));
      pave->AddText(stat);
      //sprintf(str,"%s\n stat");
 sprintf(str,"%s %s\n",str,stat);

      sprintf(stat, "Sigma_{0} = %5.2f #pm %3.2f", genp[i]->GetParameter(5), genp[i]->GetParError(5));
      pave->AddText(stat);
      //sprintf(str,"%s\n stat");
 sprintf(str,"%s %s\n",str,stat);
      sprintf(stat, "mu_avg = %5.2f #pm %3.2f", genp[i]->GetParameter(3), genp[i]->GetParError(3));
      pave->AddText(stat);
      //sprintf(str,"%s\n stat");
 sprintf(str,"%s %s\n",str,stat);
      hcommon[i]->GetListOfFunctions()->Add(pave);
  /*
  TLatex *params = new TLatex(0,0,"GP parameters:");
  params ->SetTextFont(42);
  params ->SetTextSize(0.0);
  listOfLines->Add(params);


  sprintf(stat, "#chi^{2} / ndf = %4.2f / %4.2d", genp[i]->GetChisquare(), genp[i]->GetNDF());
  TLatex *chisc = new TLatex(0,0,stat);
  chisc ->SetTextFont(42);
  chisc ->SetTextSize(0.0);
  listOfLines->Add(chisc);

  sprintf(stat, "#lambda = %4.2f #pm %3.2f", genp[i]->GetParameter(0), genp[i]->GetParError(0));
  TLatex *lambda = new TLatex(0,0,stat);
  lambda ->SetTextFont(42);
  lambda ->SetTextSize(0.0);
  listOfLines->Add(lambda);

  sprintf(stat, "Gain_{SPE} = %4.2f #pm %3.2f", genp[i]->GetParameter(1), genp[i]->GetParError(1));
  TLatex *gainspe = new TLatex(0,0,stat);
  gainspe ->SetTextFont(42);
  gainspe ->SetTextSize(0.0);
  listOfLines->Add(gainspe);

  sprintf(stat, "Mean_{0} = %5.2f #pm %3.2f", genp[i]->GetParameter(4), genp[i]->GetParError(4));
  TLatex *mean0 = new TLatex(0,0,stat);
  mean0 ->SetTextFont(42);
  mean0 ->SetTextSize(0.0);
  listOfLines->Add(mean0);

  sprintf(stat, "Sigma_{0} = %5.2f #pm %3.2f", genp[i]->GetParameter(5), genp[i]->GetParError(5));
  TLatex *sigma0 = new TLatex(0,0,stat);
  sigma0 ->SetTextFont(42);
  sigma0 ->SetTextSize(0.0);
  listOfLines->Add(sigma0);*/
    }
  }
  serv->SetItemField("/Fits", "value", str);
}

void savefile(TString arg1){
  char fname[200];
  cout<<"save to "<<arg1<<endl;
  //sprintf(fname,"%s.root",arg1);
  arg1+=".root";
  TFile fout(arg1,"recreate");
  for(int i=0;i<2;i++)
    {
  hsigspectr[i]->Write();
  hblspectr[i]->Write();
  hacsignal[i]->Write();//,*hacsignal2;
  hcommon[i]->Write();
    }
  fout.Close();
}
